/*--------------------------------------------------------------------------------------

 DMD_String.cpp - Function and support library for the Freetronics DMD, a 512 LED matrix display
           panel arranged in a 32 x 16 layout.

 Copyright (C) 2011 Marc Alexander (info <at> freetronics <dot> com)

 Note that the DMD library uses the SPI port for the fastest, low overhead writing to the
 display. Keep an eye on conflicts if there are any other devices running from the same
 SPI port, and that the chip select on those devices is correctly set to be inactive
 when the DMD is being written to.

 ---

 This program is free software: you can redistribute it and/or modify it under the terms
 of the version 3 GNU General Public License as published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------------*/
#include "DMD_String.h"

/*--------------------------------------------------------------------------------------
 Setup and instantiation of DMD library
 Note this currently uses the SPI port for the fastest performance to the DMD, be
 careful of possible conflicts with other SPI port devices
--------------------------------------------------------------------------------------*/
DMD_String::DMD_String(byte panelsWide, byte panelsHigh) : DMD (panelsWide, panelsHigh)
{
}

//DMD_String::~DMD_String()
//{
//   // nothing needed here
//}

void DMD::drawString(int bX, int bY, String sString, byte length,
		     byte bGraphicsMode)
{
    drawString(bX, bY, sString.c_str(), length, bGraphicsMode);
}

void DMD::drawString(int bX, int bY, String sString,
		     byte bGraphicsMode)
{
    drawString(bX, bY, sString, sString.length(), bGraphicsMode);
}

void DMD_String::drawMarquee(String sString, byte length, int left, int top)
{
    drawMarquee(sString.c_str(), length, left, top);
}

void DMD_String::drawMarquee(String sString, int left, int top)
{
//    drawMarquee(sString.c_str(), sString.length(), left, top);
    drawMarquee(sString, sString.length(), left, top);
}

