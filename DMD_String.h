/*--------------------------------------------------------------------------------------

 DMD_String.h   - Function and support library for the Freetronics DMD, a 512 LED matrix display
           panel arranged in a 32 x 16 layout.

 Copyright (C) 2011 Marc Alexander (info <at> freetronics <dot> com)

 Note that the DMD library uses the SPI port for the fastest, low overhead writing to the
 display. Keep an eye on conflicts if there are any other devices running from the same
 SPI port, and that the chip select on those devices is correctly set to be inactive
 when the DMD is being written to.


LED Panel Layout in RAM
                            32 pixels (4 bytes)
        top left  ----------------------------------------
                  |                                      |
         Screen 1 |        512 pixels (64 bytes)         | 16 pixels
                  |                                      |
                  ---------------------------------------- bottom right

 ---
 
 This program is free software: you can redistribute it and/or modify it under the terms
 of the version 3 GNU General Public License as published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with this program.
 If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------------*/
#ifndef DMD_String_H_
#define DMD_String_H_

//Arduino toolchain header, version dependent
#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

//SPI library must be included for the SPI scanning/connection method to the DMD
#include "pins_arduino.h"
#include <avr/pgmspace.h>
#include <SPI.h>

#include "DMD.h"


//The main class of DMD_String library functions
class DMD_String : public DMD
{
  public:
    //Instantiate the DMD
    DMD_String(byte panelsWide, byte panelsHigh);
	//virtual ~DMD_String();

  //Draw a string - passing String object and length
  void drawString( int bX, int bY, String sString, byte length, byte bGraphicsMode);

  //Draw a string - passing only String object
  void drawString( int bX, int bY, String sString, byte bGraphicsMode);

  //Draw a scrolling string - passing String object and length
  void drawMarquee( String sString, byte length, int left, int top);

  //Draw a scrolling string - passing only String object
  void drawMarquee( String sString, int left, int top);



  private:

};

#endif /* DMD_String_H_ */
