// From https://circuitdigest.com/microcontroller-projects/digital-notice-board-using-p10-led-matrix-display-and-arduino

#include <SPI.h>
#include <DMD_String.h>
#include <TimerOne.h>
#include "SystemFont5x7.h"
#include "Arial_black_16.h"

#define ROW 1
#define COLUMN 1
#define FONT Arial_Black_16

DMD_String led_module(ROW, COLUMN);

// ** This is the message to be displayed
String txtMsg = "Hello";

void scan_module()
{
  led_module.scanDisplayBySPI();
}

void setup()
{
  Timer1.initialize(2000);
  Timer1.attachInterrupt(scan_module);
  led_module.clearScreen( true );
}

void loop()
{
  led_module.selectFont(FONT);

// Original DMD object method of calling, using C strings
//  led_module.drawMarquee(txtMsg.c_str(), txtMsg.length(), (32 * ROW), 0);

// Using DMD_String, passing the String object and length
//  led_module.drawMarquee(txtMsg, txtMsg.length(), (32 * ROW), 0);

// Using DMD_String, passing only the String object, omitting the length
  led_module.drawMarquee(txtMsg, (32 * ROW), 0);

  long start = millis();
  long timing = start;
  boolean flag = false;
  while (!flag)
  {
    if ((timing + 20) < millis())
    {
      flag = led_module.stepMarquee(-1, 0);
      timing = millis();
    }
  }
}
